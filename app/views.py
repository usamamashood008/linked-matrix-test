from django.shortcuts import render, HttpResponse
from ratelimit.decorators import ratelimit


def my_key(group, request):
    if request.user.is_authenticated:
        return request.user.username
    else:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            return f"ano {x_forwarded_for.split(',')[0]}"
        else:
            return f"ano {request.META.get('REMOTE_ADDR')}"


def my_rate(group, request):
    if request.user.is_authenticated:
        user = request.user
        if user.groups.filter(name='gold'):
            return '10/m'
        elif user.groups.filter(name='silver'):
            return '5/m'
        elif user.groups.filter(name='bronze'):
            return '2/m'
    else:
        return '1/m'


@ratelimit(key=my_key, rate=my_rate, block=True)
def home(request):
    return HttpResponse('<h1>Home</h1>')

