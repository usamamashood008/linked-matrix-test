from django.test import TestCase
from django.shortcuts import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

USER = get_user_model()


class AnonymousUserTest(TestCase):

    def test_first_request(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_second_request(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 403)


class BronzeUserTest(TestCase):

    def setUp(self):
        self.user = USER.objects.create_user(username='test',
                                             password='12test12',
                                             email='test@example.com')
        self.user.save()
        group = Group.objects.create(name='bronze')
        group.user_set.add(self.user)
        group.save()

        self.client.login(username='test', password='12test12')

    def test_first_request(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_second_request(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_third_request(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 403)
