from datetime import datetime
from django.shortcuts import HttpResponse
from django.core.cache import cache
from django.http import HttpResponseForbidden
from django.core.exceptions import PermissionDenied
import logging


logger = logging.getLogger(__name__)


class IPsLogging:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        ip = self.get_client_ip(request)
        logger.info(f'{ip}')
        # cache_ip = cache.get(ip)
        # if cache_ip:
        #     if cache_ip == 'block':
        #         raise PermissionDenied
        #     current_time = datetime.now()
        #     total_sec = (current_time-cache_ip.get('time')).total_seconds()
        #     if total_sec < 60:
        #         if cache_ip.get('count') < 5:
        #             cache_data = {
        #                 'time': cache_ip['time'],
        #                 'count': cache_ip['count']+1,
        #             }
        #             cache.set(ip, cache_data,60)
        #         else:
        #             cache.set(ip, 'block', 60)
        #             raise PermissionDenied
        #     else:
        #         cache_data = {
        #             'time': datetime.now(),
        #             'count': 1
        #         }
        #         cache.set(ip, cache_data,60)
        # else:
        #     cache_data = {
        #         'time': datetime.now(),
        #         'count': 1
        #     }
        #     cache.set(ip, cache_data)

        response = self.get_response(request)
        return response

    def get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip
