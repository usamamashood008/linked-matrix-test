FROM python:3.8
ENV PYTHONUNBUFFERED 1
WORKDIR /project
COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock


COPY . .

RUN pip install pipenv
RUN pipenv install --skip-lock --system --dev

#CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]